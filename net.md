# how shit works normally

For any physical network device a corresponding network interface is
created. Names are:

- enp* for wired ethernet
- wlp* for wireless device

dhcpcd monitors the interfaces and when one becomes active, it leases
an IP address for it, so that you don't need to do anything to get your
network working.

If interface is down, try to enable it with:

```sh
ip link set dev $device up
```

# priorities

Changing priority (order) in connman:

wired:
```sh
default via 192.168.0.1 dev enp6s0
192.168.0.0/24 dev wlp8s0f3u2 proto kernel scope link src 192.168.0.102
192.168.0.0/24 dev enp6s0 proto kernel scope link src 192.168.0.16
192.168.0.1 dev enp6s0 scope link
192.168.0.1 dev wlp8s0f3u2 scope link
```

wireless:
```sh
default via 192.168.0.1 dev wlp8s0f3u2
192.168.0.0/24 dev wlp8s0f3u2 proto kernel scope link src 192.168.0.102
192.168.0.0/24 dev enp6s0 proto kernel scope link src 192.168.0.16
192.168.0.1 dev enp6s0 scope link
192.168.0.1 dev wlp8s0f3u2 scope link
```

so seems like the only difference is `default`,
but i thought priority is changed with metric.

# connecting 2 computers directly

https://superuser.com/questions/1655421/how-can-i-set-up-a-p2p-ethernet-connection

# ad hoc wifi

wifi without a router

src: https://wiki.debian.org/WiFi/AdHoc
