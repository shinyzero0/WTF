# 3D plotting with gnuplot

```sh
# read data matrix using first column and row as x and y and drawing lines between points
splot "2.2.full.csv" matrix nonuniform with lines
# set output type
set terminal png size 1200,1000 font "monospace,12"
# set output file
set output "2.2.png"
```
