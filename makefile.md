# GNU make

## automatic variables

- `$<` the first input file
- `$@` the output file
- `$($varD)` the `$var` dirname
- `$($varD)` the `$var` basename


## pattern rules

`%` is a wildcard

```makefile
all: bin/prog
bin/%: %.c
	$(CC) -o $@ $<
```

- `$*` the pattern rule matched part


## implicit rules

### C

n.o is made automatically from n.c with a recipe of the form `$(CC) $(CPPFLAGS) $(CFLAGS) -c`

### C++

n.o is made automatically from n.cc, n.cpp, or n.C with a recipe of the form `$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c`
