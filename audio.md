# audio systems

# mixing with pactl

```sh
# use completion for $id
pactl set-sink-input-mute $id {0,1,toggle}
pactl set-sink-input-volume $id $percentage
```
