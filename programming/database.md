# what if my fucking playlists are relational DB of tags?

# migrations

## sqlite-migrate

- lang: python
- src: https://github.com/simonw/sqlite-migrate

## sqitch

Migrations stored as 3 sql scripts for:

1. deploying
2. verifying
3. reverting

- lang: perl
- src: https://sqitch.org/

## alembic

- src: https://alembic.sqlalchemy.org/en/latest/
- lang: python


# event-driven and history tracking

## sqlite-history

- src: https://github.com/simonw/sqlite-history
- doc: https://simonwillison.net/2023/Apr/15/sqlite-history/
