# Shell scripting

- [ctypes](https://github.com/taviso/ctypes.sh) - bash FFI

# ignore case when matching awk
awk -v IGNORECASE=1 '/INSENSITIVE/'

# lower/upper casing

src: https://scriptingosx.com/2019/12/upper-or-lower-casing-strings-in-bash-and-zsh/

## bash

```sh
name="John Doe"
echo ${name,,}
# john doe
echo ${name^^}
# JOHN DOE
```

## zsh

expansion modifiers:

```sh
name="John Doe"
echo ${name:l}
# john doe
echo ${name:u}
# JOHN DOE
```

expansion flags:

```sh
name="John Doe"
echo ${(L)name}
john doe
echo ${(U)name}
JOHN DOE
```

typeset:
```sh
typeset -l name
name="John Doe"
echo $name
john doe
typeset -u name
echo $name
JOHN DOE
```
