# Teal, typed lua

[compiler](https://github.com/teal-language/tl)
[types](https://github.com/teal-language/teal-types)
[nvim](https://github.com/svermeulen/nvim-teal-maker)
