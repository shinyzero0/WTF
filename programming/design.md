# software design

## hyrum's law

With a sufficient number of users of an API,
it does not matter what you promise in the contract:
all observable behaviors of your system
will be depended on by somebody.

- src: https://www.hyrumslaw.com/
- xkcdL https://xkcd.com/1172/
