# Perl

# dependency management

First, install `cpanm` (cpanminus). Distributions usually have it packaged.

```sh
# install local::lib to yet unknown to cpanm default local::lib location
cpanm --local-lib=~/perl5 local::lib

# configure local::lib envars
eval $(perl -I ~/perl5/lib/perl5/ -Mlocal::lib)

# install the bundler
cpanm Carton

# cpanfile lists project dependencies
echo "requires 'Plack', '0.9980';" > cpanfile

# this downloads the dependencies to ./local/ and creates a lockfile
carton install
git add cpanfile.snapshot # lockfile

# this sets environment so that app could access the dependencies
carton exec app.pl
```

https://metacpan.org/pod/Dist::Zilla can be used instead of Carton. sqitch uses it

# HTTPS requests not working

Don't forget [Mozilla::CA](https://metacpan.org/release/Mozilla-CA) certs

# parse formdata files with PSGI/CGI

```perl
use CGI::PSGI;

CGI::PSGI->new()->upload('filename');
```;


[source](https://metacpan.org/pod/CGI#Processing-a-file-upload-field)
