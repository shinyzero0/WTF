# wireless networks

# wpa_supplicant roaming

/etc/wpa_supplicant/wpa_supplicant.conf:
```conf
bgscan="simple:30:-70:3600"
```

# Speed up DHCP by disabling ARP probing

dhcpcd contains an implementation of a recommendation of the DHCP standard
(RFC 2131) to verify via ARP if the assigned IP is not used by something
else. This is usually not needed in home networks, so it is possible to
save about 5 seconds on every connect by disabling it:

/etc/dhcpcd.conf:
```conf
noarp
```
# wpa_cli action script

daemon mode for triggering script on wpa events
