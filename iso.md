[debootstrap](./iso/install_debian_with_debootstrap_howto.md)
# making bootable isos

tools:

- xorriso for packing files into iso and making it bootable
- mkisofs (can be used as `xorriso -as mkisofs`)

# partitioning

- partition with fdisk from util-linux, everything else sucks
- use sfdisk for fdisk automation [tutorial](https://suntong.github.io/blogs/2015/12/25/use-sfdisk-to-partition-disks/)

