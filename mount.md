Bind mount on a DIR masks the current DIR content, so it will be
inaccessible for the duration of bind mount, unless the DIR was bind
mounted somewhere else. Bind mount is a kind of hard link for directories,
as it makes several directories have same inode.
