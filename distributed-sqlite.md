# Turso

src: https://turso.tech/

# LiteFS

built on hashicorp consul https://www.consul.io/

- src: https://github.com/superfly/litefs
- docs: https://fly.io/docs/litefs/how-it-works/

# marmot

built on [NATS](https://nats.io/)

- src: https://github.com/maxpert/marmot

# dqlite

Canonical

Raft consensus ./distributed.md

- src: https://dqlite.io/

# rqlite

with blackjack and hookers

- src: https://github.com/rqlite/rqlite
