# Some random guys dotfiles list

- [awesomewm](https://github.com/raven2cz/awesomewm-config)
- [tjdevres](https://github.com/tjdevries/config_manager) - cool guy, neovim, telescope, plenary maintainer
- [alex](https://github.com/alex-eg/dotfiles)
- [way of the sword](https://github.com/Hellseher/wds) - learning stuff
- [snrl](https://github.com/name-snrl/home)
- [savq](https://github.com/savq/dotfiles)
- [siph](https://github.com/siph/nix-dotfiles) - nix dotfiles
- [nick](https://github.com/nicknisi/dotfiles)
- [alex](https://github.com/AlexvZyl/.dotfiles) - Most likely the only married Arch Linux user
- [masasam](https://github.com/masasam/dotfiles) - has cool makefile
- [alex](https://github.com/alex35mil/dotfiles)
- [gh0stzk](https://github.com/gh0stzk/dotfiles)
- [dagadbm](https://github.com/dagadbm/dotfiles)
- [occivink](https://github.com/occivink/config) - kakoune, elvish, bspwm
