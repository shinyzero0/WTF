# `(Neo)?Vim` plugins & stuff

- [ragtag](https://github.com/tpope/vim-ragtag) - {x,ht}ml mappings
- [sidebar](https://github.com/sidebar-nvim/sidebar.nvim)
- [tree climber](https://github.com/drybalka/tree-climber.nvim) treesitter navigation
- [ts-tools](https://github.com/pmizio/typescript-tools.nvim) typescript integration
- [move-less](https://github.com/anschnapp/move-less) - look around without cursor moving
- [visual-split](https://github.com/wellle/visual-split.vim) - idk
- [edgemotion](https://github.com/haya14busa/vim-edgemotion)
- [haxe](https://github.com/danielo515/haxe-nvim) - write plugins in [haxe](https://haxe.org/)
- [cosmic ui](https://github.com/CosmicNvim/cosmic-ui) - lsp rename & actions wrapper
- [amend](https://github.com/anuvyklack/keymap-amend.nvim) - override a keymap conditionally
- [easypick](https://github.com/axkirillov/easypick.nvim) - telescope pickers from CLI
- [markid](https://github.com/David-Kunz/markid) - same color for same identifiers/variables
 
## CMP

- [cmp-env](https://github.com/SergioRibera/cmp-dotenv) - dotenv completion
- [rpncalc](https://github.com/PhilRunninger/cmp-rpncalc/) - RPN reverse polish notation calculator
- [zsh](https://github.com/tamago324/cmp-zsh)

## Git

[Gim](vim/gim.md)
