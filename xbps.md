# XBPS

# ignorepkg

You don't need to install dependencies if they are installed with some
other package manager (nix, guix, some shit like asdf or rtx) or manually.

You can create a file in `/etc/xbps.d/*.conf` with content like

```conf
ignorepkg=fzf
ignorepkg=zoxide
```

and fzf and zoxide will be always satisfied without installing
