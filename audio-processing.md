# audio processing

## noise reduction

### sox

```sh
# define noise
# cut a noise sample
FILE_EXTENSION=${INPUT_FILE##*.}
NOISE_SAMPLE_FILE="noise-sample.$FILE_EXTENSION"
# could use -to instead of -t and set end time
ffmpeg -accurate_seek -i $INPUT_FILE -ss $START_TIME -t $DURATION "$NOISE_SAMPLE_FILE"
sox "$NOISE_SAMPLE_FILE" -n noiseprof noise_profile

# tweak this value
NOISERED_AMOUNT=0.1
sox $INPUT_FILE ${INPUT_FILE%.*}.noisered.$FILE_EXTENSION noisered noise-profile $NOISERED_AMOUNT
# or to play immediately (the player sucks)
play $INPUT_FILE noisered noise-profile $NOISERED_AMOUNT
```

the same in one line:

```sh
sox $INPUT_FILE -n trim $START_TIME $DURATION noiseprof \
	| play $INPUT_FILE noisered $NOISERED_AMOUNT
```

### rnnoise & denoiseit

i got [denoiseit](https://github.com/DragoonAethis/DenoiseIt/) packaged in my guix channel

script for denoising

```sh
#!/bin/bash
for input_file in "$@"; do
    file_name="${input_file%.*}"
    # Convert file to ".wav" 48k (needed by rnnnoise)
    ffmpeg -threads 2 -y -i "$input_file" -vn -ar 48000 "${file_name}.48k.wav"
    # Actual denoise
    denoiseit "${file_name}.48k.wav" "${file_name}.rnnoise.wav";
    
    # Convert to ogg
    ffmpeg -threads 2 -y -i "${file_name}.rnnoise.wav" "${file_name}.rnnoise.ogg"
    
    # Remove wav temporary files
    rm "${file_name}.48k.wav"
    rm "${file_name}.rnnoise.wav"
done;

```
